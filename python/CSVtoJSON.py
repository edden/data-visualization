import os
import csv
import json
from collections import Counter

currentPath = os.getcwd()


def csv_to_json():
    data_dict = {
        'filters': {},
        'data': {}
    }

    with open(currentPath + '/python/Future.csv', encoding='utf-8') as csv_file:
        csv_reader = csv.DictReader(csv_file)

        words_list = []
        for row in csv_reader:
            if row[list(row)[1]] == '':
                continue
            if row[list(row)[1]] not in data_dict['data']:
                data_dict['data'][row[list(row)[1]]] = {}
            if row[list(row)[0]] not in data_dict['data'][row[list(row)[1]]]:
                data_dict['data'][row[list(row)[1]]][row[list(row)[0]]] = {}
            for attr in row:
                if attr != list(row)[0] and attr != list(row)[1]:
                    data_dict['data'][row[list(row)[1]]][row[list(row)[0]]
                                                         ][attr] = row[attr]

                if attr in list(row)[4:10]:
                    words_list.extend([x.strip(',.-:()')
                                      for x in row[attr].lower().split(' ')])

        for i in range(len(words_list)):
            words_list[i] = words_list[i].removesuffix("'s")
            words_list[i] = words_list[i].replace('ies', 'y')
            if words_list[i].endswith('sses'):
                words_list[i] = words_list[i][:-2]
            if words_list[i].endswith('es'):
                words_list[i] = words_list[i][:-1]
            if not words_list[i].endswith('ss') and not words_list[i].endswith('us'):
                words_list[i] = words_list[i].removesuffix("s")
            if words_list[i] == 'worker' or words_list[i] == 'higher':
                words_list[i] = words_list[i].removesuffix("er")

        data_dict['filters'] = dict(Counter(words_list))
        data_dict['filters'] = dict(
            sorted(data_dict['filters'].items(), key=lambda item: item[1]))
        data_dict['filters'] = {key: val for key,
                                val in data_dict['filters'].items() if val > 2}

        for word in unwanted_list:
            if word in data_dict['filters']:
                data_dict['filters'].pop(word)

    with open(currentPath + '/src/lib/future.json', 'w', encoding='utf-8') as json_file_handler:
        json_file_handler.write(json.dumps(data_dict, indent=4))

    print('done!')


unwanted_list = [
    'a',
    'i',
    'at',
    'one',
    'each',
    'small',
    'which',
    'object',
    'need',
    'task',
    'content',
    'talent',
    'new',
    'like',
    'be',
    'use',
    'up',
    'can',
    'using',
    'will',
    'is',
    'other',
    'their',
    'all',
    'become',
    'used',
    'more',
    'etc',
    'by',
    'as',
    'from',
    'are',
    'on',
    'with',
    '&',
    'for',
    'in',
    'the',
    'of',
    'to',
    'and',
    'e.g',
    '2030',
    ''
]


csv_to_json()
