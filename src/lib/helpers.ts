import future from "$lib/future.json";

export const dates = ['now','2025',	'2030',	'2040',	'2050',	'beyond']

export function getHue(frontier: string) {
  const currentKey = Object.keys(future.data).findIndex(key => key === frontier)
  return 10 + (currentKey * 365) / Object.keys(future.data).length - 1
}

export function getLighting(date: string) {
  const currentKey = dates.findIndex(key => key === date)
  return 50 + currentKey * 7.5
}

export function getTag(currentWord: string) {
  let word = currentWord
  word = word.split(`'`)[0]
  word = word.replace("ies", "y").toLowerCase();
  word = word.replace(/[,.-:()]/g, "").toLowerCase();
  if(word.slice(-4) === 'sses') word = word.substring(0, word.length - 2)
  if(word.slice(-2) === 'es') word = word.substring(0, word.length - 1)
  if(word.slice(-1) === 's' && word.slice(-2) !== 'ss' && word.slice(-2) !== 'us') word = word.substring(0, word.length - 1)
  if(word === 'worker' || word === 'higher') word = word.substring(0, word.length - 2)
  return word
}